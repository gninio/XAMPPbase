#
# Package building RestFrames
#
include(${CMAKE_ROOT}/Modules/ExternalProject.cmake)
set_directory_properties(PROPERTIES EP_BASE "${CMAKE_BINARY_DIR}/Externals/")

# Externals needed by the build of our externals:
find_package( ROOT REQUIRED COMPONENTS Core Tree RIO Hist Physics )

# Temporary directory for the build results:
set( _restFramesBuildDir ${CMAKE_BINARY_DIR}/Externals/RestFramesBuild )

# Set up the variables that the users can pick up RestFrame with:
set( RESTFRAMES_INCLUDE_DIRS
   $<BUILD_INTERFACE:${_restFramesBuildDir}/include>
   $<INSTALL_INTERFACE:include>
   ${ROOT_INCLUDE_DIRS} )

set( RESTFRAMES_LIBRARIES )
foreach( lib RestFrames )
   list( APPEND RESTFRAMES_LIBRARIES
      $<BUILD_INTERFACE:${_restFramesBuildDir}/lib/${CMAKE_SHARED_LIBRARY_PREFIX}${lib}${CMAKE_SHARED_LIBRARY_SUFFIX}>
      $<INSTALL_INTERFACE:lib/${CMAKE_SHARED_LIBRARY_PREFIX}${lib}${CMAKE_SHARED_LIBRARY_SUFFIX}> )
endforeach()

# Set up the build of RestFrames for the build area:
ExternalProject_Add( RestFrames
   PREFIX ${CMAKE_BINARY_DIR}
   URL https://github.com/crogan/RestFrames/archive/v1.0.2.tar.gz
   URL_MD5 ccb037001f34e67514d19326bc1e2eae
   INSTALL_DIR ${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}
   CONFIGURE_COMMAND <SOURCE_DIR>/configure
   --prefix=${_restFramesBuildDir} --enable-shared --disable-static
   --with-rootsys=${ROOTSYS}
   BUILD_IN_SOURCE 1
   INSTALL_COMMAND COMMAND make install
   COMMAND ${CMAKE_COMMAND} -E copy_directory
   ${_restFramesBuildDir}/ <INSTALL_DIR>
   #BUILD_BYPRODUCTS ${RESTFRAMES_LIBRARIES} 
)


# Install RestFrames:
install( DIRECTORY ${_restFramesBuildDir}/
   DESTINATION . USE_SOURCE_PERMISSIONS )

# Clean up:
unset( _restFramesBuildDir )
