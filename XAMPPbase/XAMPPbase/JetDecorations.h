/// Here we define additional decorations which are
/// defined in case of jets.
/// These extend the set defined in the
/// ParticleDecorations class.

#ifndef XAMPP__JETDECORATIONS__H
#define XAMPP__JETDECORATIONS__H

#include <XAMPPbase/ParticleDecorations.h>
#include <xAODTruth/TruthParticleContainer.h>

namespace XAMPP {
    class JetDecorations : public ParticleDecorations {
    public:
        JetDecorations();
        virtual void populateDefaults(SG::AuxElement& ipart) override;
        // is this a bad jet?
        Decoration<char> isBadJet;
        // did this jet pass b-tagging?
        Decoration<char> isBJet;
        // did this jet pass the loose b-tagging
        Decoration<char> isBJet_Loose;
        // does jet pass the JVT?
        Decoration<char> passJVT;
        // does jet pass the fJVT
        Decoration<char> passfJVT;

        // number of tracks in jet
        Decoration<int> nTracks;
        // jet algorithm?
        Decoration<int> jetAlgorithm;
        // b-tag weight
        Decoration<double> BTagWeight;
        // DAOD-level decoration used for bad jet cleaning
        Decoration<char> DFCommonJets_jetClean_LooseBad;
        // the raw JVT (possibly recomputed) that we cut on
        Decoration<float> rawJVT;
        // Detector eta used for forward JVT discrimination
        Decoration<float> DetectorEta;
    };
}  // namespace XAMPP
#endif  // XAMPP__JETDECORATIONS__H
