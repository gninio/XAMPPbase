# Our workflow will define to stages: build and deploy the web site
# These stages will be attached to jobs and define the sequence in which they will be executed
#
# This example shows how to build doxygen documentation and deploy it to a DFS website.


stages:
  - format_code
  - build
  - test
  - deploy

format_code:
  stage: format_code
  image: gitlab-registry.cern.ch/xampp/atlas-code-format
  variables: {
    clang_dirs: 'XAMPPbase',  # clang-format headers and source files
    yapf_dirs: 'XAMPPbase'      # yapf-format python files
  }
  before_script:
    - git config --global user.name "xampp" && git config --global user.email "xampp.doxy@cern.ch"
    - git checkout -b ${CI_COMMIT_REF_NAME} || echo "Branch ${CI_COMMIT_REF_NAME} already exists, checking it out" && git checkout ${CI_COMMIT_REF_NAME}
  script:
    - /scripts/format_code
  only:
    - master
# Build docker image used for tests
build_docker:
  stage: build
  variables:
    TO: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_NAME
    # all submodules will be cloned recursively upon start of CI job
    GIT_SUBMODULE_STRATEGY: recursive
    GIT_SSL_NO_VERIFY: "true"
  tags:
    - docker-image-build
  script:
    - ignore

# Example job to build doxygen documenation with gitlab. Copy and adapt to your specifc case.
# By default it is commented out to not collide with the other build stages.
# Heavily based on: https://gitlab.com/pages/doxygen/
doxygen:
  stage: build
  # The centrally provided worker has doxygen installed
  image: gitlab-registry.cern.ch/ci-tools/ci-worker:cc7
  script:
    # Prepare the documentation
    - doxygen XAMPPbase/XAMPPbase.doxyfile
    # Copy the generated web to the local public/ directory
    - mv documentation/html/ public/
  # Upload the public/ directory as artifact. The web generated will be attached to the build
  artifacts:
    paths:
      - public

# Test code
test_cutflow:
  stage: test
  image: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_NAME
  tags:
    - cvmfs
  script:
    - source /home/atlas/release_setup.sh
    - cd /xampp/XAMPPbase && ls
    - bash /xampp/XAMPPbase/XAMPPbase/test/test_mc_ttbar.sh
    - diff -w test_job/cutflow.txt XAMPPbase/test/reference_cutflow.txt || exit 1


test_data:
  stage: test
  image: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_NAME
  tags:
    - cvmfs
  script:
    - source /home/atlas/release_setup.sh
    - cd /xampp/XAMPPbase && ls
    - bash /xampp/XAMPPbase/XAMPPbase/test/test_data18.sh
    - diff -w test_job/cutflow.txt XAMPPbase/test/reference_cutflow_data.txt || exit 1

test_systematics:
  stage: test
  image: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_NAME
  tags:
    - cvmfs
  script:
    - source /home/atlas/release_setup.sh
    - cd /xampp/XAMPPbase && ls
    - bash /xampp/XAMPPbase/XAMPPbase/test/test_mc_ttbar_syst.sh

test_truth:
  stage: test
  image: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_NAME
  tags:
    - cvmfs
  script:
    - source /home/atlas/release_setup.sh
    - cd /xampp/XAMPPbase && ls
    - bash /xampp/XAMPPbase/XAMPPbase/test/test_mc_ttbar_truth.sh
    - diff -w test_job/cutflow.txt XAMPPbase/test/reference_cutflow_truth.txt || exit 1

# Deploy the pages generated to DFS
dfsdeploy:
  # Executed during the deploy stage
  stage: deploy
  # Only when the master branch is pushed
  only:
    - comic_book_seller
  # Custom docker image providing the needed tools to deploy in DFS
  image: gitlab-registry.cern.ch/ci-tools/ci-web-deployer:latest
  script:
  # Script that performs the deploy to DFS. Makes use of the variables defined in the project
  # It will not sync the generated content with the folder in DFS (risk of deleting DFS management files)
  # It will just override the contents of DFS or copy new files
  - deploy-dfs
  
  
make_master_latest:
  stage: deploy
  only:
  - master
  script:
  - echo "Obtain token to authenticate with the gitlab registry"
  - JWT_PULL_PUSH_TOKEN=$(curl -q -u gitlab-ci-token:${CI_JOB_TOKEN} "https://${GITLAB_HOST}/jwt/auth?service=container_registry&scope=repository:${CI_PROJECT_PATH}:pull,push" | cut -d\" -f4 )
  - IMAGE_NAME=$(echo ${CI_PROJECT_PATH}|tr [:upper:] [:lower:])
  - 'curl "https://${CI_REGISTRY}/v2/${IMAGE_NAME}/tags/list" -H "Authorization: Bearer ${JWT_PULL_PUSH_TOKEN}"'
  - echo "Pulling the manifest of tag:${OLDTAG}"
  - 'curl "https://${CI_REGISTRY}/v2/${IMAGE_NAME}/manifests/${OLDTAG}" -H "Authorization: Bearer ${JWT_PULL_PUSH_TOKEN}" -H "accept: application/vnd.docker.distribution.manifest.v2+json" > manifest.json'
  - 'echo "Pushing new tag: ${NEWTAG}"'
  - 'curl -XPUT "https://${CI_REGISTRY}/v2/${IMAGE_NAME}/manifests/${NEWTAG}" -H "Authorization: Bearer ${JWT_PULL_PUSH_TOKEN}" -H "content-type: application/vnd.docker.distribution.manifest.v2+json" -d "@manifest.json" -v'
  - echo "List of tags in registry"
  - 'curl "https://${CI_REGISTRY}/v2/${IMAGE_NAME}/tags/list" -H "Authorization: Bearer ${JWT_PULL_PUSH_TOKEN}"'
  variables:
    GITLAB_HOST: gitlab.cern.ch
    OLDTAG: "${CI_COMMIT_REF_NAME}"
    NEWTAG: "latest"

