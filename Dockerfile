FROM atlas/athanalysis:21.2.150

ADD . /xampp/XAMPPbase
WORKDIR /xampp/build
RUN source ~/release_setup.sh &&  \
    sudo chown -R atlas /xampp && \
    cmake ../XAMPPbase && \
    make -j4

